from django.shortcuts import redirect
from django.conf.urls import url
from django.urls import path, reverse
from .views import index, insert

app_name = 'request'
urlpatterns = [
	path('', index, name="daftar"),
    path('<int:pk>/', index, name=app_name),
    path('<int:pk>/add/', insert, name="add"),
    
]
