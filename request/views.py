from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse
from datetime import datetime, timedelta
# Create your views here.
from django.views.decorators.http import require_http_methods

from login.forms import LoginForm
from login.models import DocToUser, Doctor, Patient, Riwayat, Spesialisasi


@login_required(login_url='/login/')
def index(request, pk=-1):
	if pk == -1:
		return redirect('/daftarDokter/')
	print(request.user)
	patient = Patient.objects.get(user=request.user)		
	response = {}
	response['saldo'] = "Rp. "+"{:,}".format(patient.saldo)+",00"
	jadwal = ""
	hari = ['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
	html = 'request/request.html'
	akun = DocToUser.objects.get(id=pk)
	doctor = Doctor.objects.get(user_id=akun.id)
	riwayats = Riwayat.objects.all()
	response['user'] = request.user
	response['doctor'] = doctor
	response['pk'] = pk

	waktu = datetime.strptime('09:00','%H:%M')
	for x in range(7):
		jadwal += "<tr>"
		for y in range(6):
			string = waktu.strftime('%H:%M')
			jadwal += '<td><label><input type="radio" name="waktu" value="'+str(y)+' '+string+'">'+string+'</label></td>'
		jadwal += "</tr>"
		waktu = waktu + timedelta(hours=1)
	response['jadwal'] = jadwal
	
	day = datetime.now().strftime('%d/%b/%Y')
	dt = datetime.strptime(day, '%d/%b/%Y')
	start = dt - timedelta(days=dt.weekday())
	end = start + timedelta(days=6)
	
	response['start'] = start.strftime('%d %B, %Y')
	response['end'] = end.strftime('%d %B, %Y')
	
	return render(request, html, response)

def insert(request,pk):
	print(request.method)
	if request.method == "POST":
		akun = DocToUser.objects.get(id=pk)
		doctor = Doctor.objects.get(user_id=akun.id)
		patient = Patient.objects.get(user=request.user)
		print(pk)
		print(patient)
		print(doctor)
		ke = len(Riwayat.objects.all())
		if( ke > 0 ):
			ke = Riwayat.objects.all()[ke-1].no_urut+1
		opsi = request.POST['opsi']
		saldo = patient.saldo
		delta = int(request.POST['waktu'].split(" ")[0])
		day = datetime.now().strftime('%d/%b/%Y')
		dt = datetime.strptime(day, '%d/%b/%Y')
		start = dt - timedelta(days=dt.weekday())
		date = start + timedelta(days=delta)
		date = date.strftime('%Y-%m-%d')
		print(date)

		time = request.POST['waktu'].split(" ")[1]
		try:
			Riwayat.objects.get(pasien=patient, dokter=doctor, tanggal=date, waktu=time)
			messages.error(request, "Jadwal yang anda pilih tidak tersedia !")
			return redirect('/request/'+str(pk))
		except:
			pass
		fee = getTarif(opsi)

		if (patient.saldo < fee) :
			messages.error(request, "Saldo anda kurang mencukupi dari opsi yang anda pilih.")
			return redirect('/request/'+str(pk))

		Riwayat.objects.create(no_urut=ke, pasien=patient, opsi=opsi, dokter=doctor, tanggal=date, waktu=time, tarif=fee)
		print(len(Riwayat.objects.all()))
		print("Sukses Mengirimkan Request ke Dokter!")
		messages.success(request, "Sukses Mengirimkan Request ke Dokter!")
		return redirect('/riwayat/')
	return HttpResponseRedirect(reverse('request:daftar'))

def getTarif(opsi):
	switch={
		'chat': 5000,
		'phone': 10000,
		'video': 100001
	}
	return switch[opsi]