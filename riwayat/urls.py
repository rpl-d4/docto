from django.conf.urls import url
from django.urls import path
from .views import riwayat_view, riwayatDelete

urlpatterns = [
    path('', riwayat_view, name='riwayat'),
    path('<int:id>', riwayatDelete, name='riwayatDelete'),
]
