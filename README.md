# DocTo (D4)
CSCM603125 - Software Engineering (Rekayasa Perangkat Lunak) final project repository

## Getting Started (Setup) on Ubuntu
1. `git clone https://gitlab.com/rpl-d4/docto`
2. `cd docto/`
3. Make sure you already installed `pip` or `pip3`
4. Make sure you already installed Python >= 3.5.0
5. Install virtualenv: `[sudo] pip install virtualenv`
6. `virtualenv venv`
7. `pip3 install -r requirements.txt` or `pip install -r requirements.txt`
8. `python3 manage.py runserver` or `python manage.py runserver` -> Server will run on localhost:8000
9. Start developing!

## Workflow
1. Pull any updates from `origin`
2. Make a new branch for every new feature, the convention for a new feature branchname is as follows:
	- `<your_name>/<feature_name>`
3. Commit frequently and please do write meaningful commit message
4. Push frequently to your branch
5. When your work is ready, please open a merge request in GitLab and ask for codereview
6. **Happy coding! :)**


## Admin Credentials on Production
- username: admin
- password: rpl-d4-docto

## About Us
- Albertus Angga Raharja - 1606918401
- Faraya Agatha Putri - 1606918502
- Andika Hanavian Atmam - 1606918585
- Mohammad Ammar Ramadhan - 1606918591
- Donny Samuel - 1606918603
