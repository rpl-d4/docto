from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from login.models import DocToUser, Doctor, Konsultasi
# Create your views here.
@login_required(login_url="/login/")
def index(request):
	response = {}
	doc = Doctor.objects.all()
	con = Konsultasi.objects.all()
	response['doctors'] = doc
	response['consulate'] = con
	return render(request, 'doclist.html', response)