from django.conf.urls import url
from django.urls import path
from .views import index

app_name = 'daftarDokter'
urlpatterns = [
    path('', index, name=app_name),
]