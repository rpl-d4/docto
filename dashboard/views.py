from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from login.models import Doctor, Spesialisasi

response = {}

# Create your views here.
@login_required(login_url="/login/")
def index(request):
	html = 'dashboard/dashboard.html'
	docs = Doctor.objects.all()
	spesialisasi = Spesialisasi.objects.all()
	namaDokter = request.GET.get('inputNamaDokter')
	waktu = request.GET.get('inputWaktu')
	spesialisasiDokter = request.GET.get('inputSpesialisasi')
	hari = request.GET.get('inputHari')

	listOfSpesialisasi=[]

	for i in spesialisasi :
		listOfSpesialisasi.append(i.nama)

	if namaDokter:
		docs = docs.filter(nama__icontains=namaDokter)
	
	if spesialisasiDokter:
		if spesialisasiDokter!="0":
			docs = docs.filter(spesialisasi__nama__iexact=spesialisasiDokter)

	if hari:
		if hari!="all":
			docs = docs.filter(hari__iexact=hari)

	response['docs'] = docs
	response['spesialisasi'] = listOfSpesialisasi

	return render(request, html, response)

