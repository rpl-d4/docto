from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin

from login.forms import PatientCreationForm, DoctorCreationForm
from login.models import DocToUser, Doctor, Patient, Spesialisasi, Konsultasi


class DocToAdmin(UserAdmin):
    model = DocToUser
    fieldsets = tuple(filter(lambda fieldset: fieldset[0] != "Personal info", UserAdmin.fieldsets)) + (("Custom Role", {
        "fields": ("is_doctor", "is_patient")
    }),)


class PatientAdmin(UserAdmin):
    model = DocToUser
    add_form = PatientCreationForm


class DoctorAdmin(UserAdmin):
    model = DocToUser
    add_form = DoctorCreationForm


admin.site.register(DocToUser, DocToAdmin)
admin.site.register(Doctor)
admin.site.register(Patient)
admin.site.register(Spesialisasi)
admin.site.register(Konsultasi)
