from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.password_validation import get_default_password_validators
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import (
    check_password,
)
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from login.models import DocToUser, Doctor, Patient
from .models import DocToUser, Patient


class LoginForm(forms.Form):
    username = forms.CharField(validators=[UnicodeUsernameValidator], widget=forms.TextInput(attrs={'class': 'input'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'input'}))

    def clean(self):
        super().clean()

        cleaned_data = self.cleaned_data
        username, password = cleaned_data['username'], cleaned_data['password']

        query_set = DocToUser.objects.filter(username=username)

        if query_set.count() == 0:
            username_does_not_exist_error = ValidationError(_("Couldn't find your account."))
            self.add_error('username', username_does_not_exist_error)
            raise username_does_not_exist_error

        user = query_set[0]
        if not check_password(password, user.password):
            wrong_password_error = ValidationError(_("Wrong password."))
            self.add_error('password', wrong_password_error)
            raise wrong_password_error
        return cleaned_data


class PatientForm(forms.ModelForm):
    email = forms.EmailField(
        label=_("Email"),
        widget=forms.EmailInput,
    )
    nama = forms.CharField(
        label=_("Nama"),
        widget=forms.TextInput
    )
    tanggal_lahir = forms.DateField(
        label=_("Tanggal Lahir"),
        widget=forms.DateInput
    )
    no_telp = PhoneNumberField(widget=PhoneNumberPrefixWidget, label="Nomor Telepon")
    alamat = forms.CharField(
        label=("Alamat"),
        widget=forms.TextInput
    )
    saldo = forms.DecimalField(
        label=("Saldo"),
        widget=forms.NumberInput,
    )

    class Meta(UserCreationForm.Meta):
        model = Patient
        fields = (
            'email', 'nama', 'tanggal_lahir', 'no_telp', 'alamat', 'saldo'
        )


class PatientCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = DocToUser
        fields = '__all__'

    def save(self, commit=True):
        user = UserCreationForm.save(commit=False)
        user.is_patient = True
        user.save()
        patient = Patient.objects.create(user=user, *self.cleaned_data)
        return user


class DoctorCreationForm(UserCreationForm):
    nama = forms.CharField(
        label=_("Nama"),
        widget=forms.TextInput
    )
    no_telp = PhoneNumberField(widget=PhoneNumberPrefixWidget, label="Nomor Telepon")
    pendidikan = forms.CharField(
        label=("Pendidikan"),
        widget=forms.TextInput
    )
    waktu_pengalaman = forms.DecimalField(
        label=("Waktu Pengalaman"),
        widget=forms.NumberInput
    )
    foto = forms.CharField(
        label=("URL Foto"),
        widget=forms.TextInput,
    )

    class Meta(UserCreationForm.Meta):
        model = DocToUser
        fields = '__all__'

    def save(self, commit=True):
        user = UserCreationForm.save(commit=False)
        user.is_doctor = True
        user.save()
        doctor = Doctor.objects.create(user=user, *self.cleaned_data)
        return user

