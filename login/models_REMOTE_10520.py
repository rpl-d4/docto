from datetime import date

from django.contrib.auth.models import AbstractUser
from django.core.validators import URLValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
class DocToUser(AbstractUser):
    is_doctor = models.BooleanField(default=False)
    is_patient = models.BooleanField(default=False)

    class Meta:
        verbose_name = "DocToUser"


class Spesialisasi(models.Model):
    nama = models.CharField(max_length=100, default="Spesialisasi")

    class Meta:
        verbose_name = "Spesialisasi"

    def __str__(self):
        return "Spesialisasi: " + self.nama


class Doctor(models.Model):
    user = models.OneToOneField(DocToUser, on_delete=models.CASCADE)
    nama = models.CharField(max_length=200)
    hari = models.CharField(max_length=20, default="Senin")
    no_telp = PhoneNumberField()
    spesialisasi = models.OneToOneField(Spesialisasi, on_delete=models.CASCADE)
    pendidikan = models.CharField(max_length=100)
    waktu_pengalaman = models.DecimalField(decimal_places=2, max_digits=3)
    foto = models.CharField(max_length=200, validators=[URLValidator])

    class Meta:
        verbose_name = "Doctor"

    def __str__(self):
        return "Doctor: " + self.user.username + "-" + self.nama


class Patient(models.Model):
    user = models.OneToOneField(DocToUser, on_delete=models.CASCADE)

    nama = models.CharField(max_length=200)

    tanggal_lahir = models.DateField()

    @property
    def usia(self):
        today, born = date.today(), self.tanggal_lahir
        return today.year - self.tanggal_lahir.year - ((today.month, today.day) < (born.month, born.day))

    no_telp = PhoneNumberField()
    alamat = models.CharField(max_length=200)
    saldo = models.BigIntegerField()

    class Meta:
        verbose_name = "Patient"

    def __str__(self):
        return "Patient: " + self.user.username + "-" + self.nama


@receiver(post_save, sender=Patient)
def set_user_is_patient_true(sender, instance, **kwargs):
    print("Post signal patient " + str(instance))
    instance.user.is_patient = True
    instance.user.is_doctor = False
    instance.user.save()


@receiver(post_save, sender=Doctor)
def set_user_is_doctor_true(sender, instance, **kwargs):
    print("Post signal doctor" + str(instance))
    instance.user.is_doctor = True
    instance.user.is_patient = False
    instance.user.save()
