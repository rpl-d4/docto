# Generated by Django 2.1.3 on 2018-12-11 16:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0007_merge_20181211_2301'),
    ]

    operations = [
        migrations.CreateModel(
            name='Konsultasi',
            fields=[
                ('no_urut', models.IntegerField(primary_key=True, serialize=False)),
                ('hari', models.CharField(max_length=200)),
                ('jam_mulai', models.TimeField()),
                ('jam_selesai', models.TimeField()),
                ('lokasi', models.CharField(max_length=200)),
                ('dokter', models.ForeignKey(db_column='dokter', on_delete=django.db.models.deletion.DO_NOTHING, to='login.Doctor')),
            ],
            options={
                'verbose_name': 'Konsultasi',
            },
        ),
    ]
